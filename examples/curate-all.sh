#!/bin/sh

set -eu

PATH="$PATH:$PWD/../bin"

BASEDIR=/var/www/html

#KODI_RELEASES='dharma  eden  frodo  gotham  helix  isengard  jarvis  krypton  leia'
KODI_RELEASES=$(GET http://mirrors.kodi.tv/addons/ | grep -Po 'href="\K[a-z]+(?=/")') #'

for release in $KODI_RELEASES; do
	curate --basedir "$BASEDIR" \
		--format kodi --output "kodi/$release/addons.xml.gz" "$@" \
		"http://mirrors.kodi.tv/addons/$release/addons.xml.gz"
	md5sum "$BASEDIR/kodi/$release/addons.xml.gz" \
		> "$BASEDIR/kodi/$release/addons.xml.gz.md5"
done

curate --basedir "$BASEDIR" \
	--format hydrogen --output hydrogen/addons.xml.gz "$@" \
	http://www.hydrogen-music.org/feeds/drumkit_list.php
gunzip --keep --force ~/.local/share/curate/hydrogen/addons.xml.gz
